//! Implementation details for block showing number of updatable pacman packages

use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use alpm::{Alpm, SigLevel};
use anyhow::Error;
use async_trait::async_trait;

use super::Block;

/// Packages block private data
pub struct Packages {
    /// Enabled package database names
    dbs: Vec<String>,
}

impl Packages {
    /// Create a new [`Packages`] instance
    ///
    /// # Errors
    ///
    /// Fails if `/etc/pacman.conf` is unreadable
    pub fn new() -> Result<Self, Error> {
        let lines = BufReader::new(File::open("/etc/pacman.conf")?).lines();

        let mut dbs = vec![];
        for line in lines {
            let line = line?;

            if line.starts_with('[') && line != "[options]" {
                dbs.push(String::from(&line[1..line.len() - 1]));
            }
        }

        Ok(Self { dbs })
    }
}

#[async_trait]
impl Block for Packages {
    /// Update number of currently updatable packages
    ///
    /// # Errors
    ///
    /// If failed to register package sync databases or `HOME` environment variable is not set
    async fn run(&mut self) -> Result<String, Error> {
        let handle = Alpm::new("/", "/var/lib/pacman/")?;

        for db in &self.dbs {
            handle.register_syncdb(&db[..], SigLevel::NONE)?;
        }

        let syncdbs = handle.syncdbs();
        let num_packages = handle
            .localdb()
            .pkgs()
            .iter()
            .filter(|pkg| pkg.sync_new_version(syncdbs).is_some())
            .count();

        let icon = if Path::new(&(env::var("HOME")? + "/.cache/update/syncing")).exists() {
            "🔃"
        } else {
            "📦"
        };

        Ok(format!("{icon} {num_packages}"))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Packages};

    #[test]
    fn test_new() {
        Packages::new().expect("Failed to create Packages block");
    }

    #[tokio::test]
    async fn test_run() {
        Packages::new()
            .expect("Failed to create Packages block")
            .run()
            .await
            .expect("Failed to check for package updates");
    }
}
