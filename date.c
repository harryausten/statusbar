// SPDX-License-Identifier: MIT
#include <errno.h>
#include <stdio.h>
#include <time.h>

#include "block.h"

int date_exec(char *buf)
{
	snprintf(buf, 6, "%c%c%c%c ", 0xf0, 0x9f, 0x93, 0x85);
	time_t now = time(NULL);

	if (strftime(&buf[5], MAX_BLOCK_SIZE - 5,
		"%a %d %b %T", localtime(&now)))
		return 0;

	return -ENOMEM;
}
