// SPDX-License-Identifier: MIT
/*
 * Battery percentage block
 *
 * Copyright (c) 2020 Harry Austen
 */

#include <errno.h>
#include <libudev.h>
#include <stdio.h>
#include <string.h>

#include "block.h"

static struct udev *udev;
static struct udev_enumerate *enumerate;

int battery_setup(void)
{
	int ret;

	udev = udev_new();
	if (!udev)
		return -errno;

	enumerate = udev_enumerate_new(udev);
	if (!enumerate)
		return -errno;

	ret = udev_enumerate_add_match_subsystem(enumerate, "power_supply");
	if (ret)
		return ret;

	ret = udev_enumerate_add_match_sysattr(enumerate, "type", "Battery");
	if (ret)
		return ret;

	ret = udev_enumerate_scan_devices(enumerate);
	return ret;
}

int battery_populate_string(char *buf,
			    const char *capacity, const char *status)
{
	char c = (strcmp(status, "Charging")) ? 0x87 : 0x86;
	int ret = snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c %s%%",
				0xe2, 0xac, c, capacity);

	if (ret < 0)
		return ret;

	return 0;
}

int battery_read(char *buf)
{
	int ret = -ENODEV;
	struct udev_list_entry *devices;
	struct udev_list_entry *entry;

	devices = udev_enumerate_get_list_entry(enumerate);

	udev_list_entry_foreach(entry, devices) {
		const char *path = udev_list_entry_get_name(entry);
		struct udev_device *dev =
			udev_device_new_from_syspath(udev, path);
		const char *capacity =
			udev_device_get_sysattr_value(dev, "capacity");
		const char *status =
			udev_device_get_sysattr_value(dev, "status");

		if (capacity && status)
			ret = battery_populate_string(buf, capacity, status);
		udev_device_unref(dev);
		if (!ret)
			break;
	}

	return ret;
}

int battery_exec(char *buf)
{
	int ret;

	ret = battery_setup();
	if (ret)
		goto battery_error;

	ret = battery_read(buf);
	if (ret)
		buf[0] = '\0';

battery_error:
	if (enumerate)
		udev_enumerate_unref(enumerate);
	if (udev)
		udev_unref(udev);

	return ret;
}
