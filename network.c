// SPDX-License-Identifier: MIT
#include <arpa/inet.h>
#include <errno.h>
#include <linux/wireless.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "block.h"

static inline bool interesting_iface(int fd, struct ifreq *iface)
{
	return (ioctl(fd, SIOCGIFFLAGS, iface) != -1)
		&& (iface->ifr_flags & IFF_UP)
		&& !(iface->ifr_flags & IFF_LOOPBACK);
}

static inline const char *iface_to_ip(struct ifreq *iface)
{
	return inet_ntoa(((struct sockaddr_in *)&iface->ifr_addr)->sin_addr);
}

static int get_iface(int fd, char *if_name, char *ip)
{
	struct ifreq ifs[10];
	struct ifconf ifc = {
		.ifc_len = 10 * sizeof(struct ifreq),
		.ifc_req = ifs,
	};

	if (ioctl(fd, SIOCGIFCONF, &ifc) == -1)
		return -errno;

	for (struct ifreq *cur = ifc.ifc_req;
			cur < ifc.ifc_req + ifc.ifc_len / sizeof(*cur); ++cur) {
		if (interesting_iface(fd, cur)) {
			strncpy(ip, iface_to_ip(cur), INET_ADDRSTRLEN);
			strncpy(if_name, cur->ifr_name, IFNAMSIZ);
			break;
		}
	}

	if ((if_name[0]) && (ip[0]))
		return 0;

	return -ENODEV;
}

static int get_ssid(int fd, const char *if_name, char *essid)
{
	struct iwreq iw = {
		.u.essid = {
			.pointer = essid,
			.length = IW_ESSID_MAX_SIZE,
		},
	};
	strncpy(iw.ifr_ifrn.ifrn_name, if_name, IFNAMSIZ);

	if (ioctl(fd, SIOCGIWESSID, &iw) == -1)
		return -errno;

	return 0;
}

int network_exec(char *buf)
{
	int ret;
	int fd;
	char if_name[IFNAMSIZ] = "";
	char ip[INET_ADDRSTRLEN + 1] = "";
	char essid[IW_ESSID_MAX_SIZE] = "";

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		ret = -errno;
		goto exit;
	}

	ret = get_iface(fd, if_name, ip);
	if (ret)
		goto exit;

	if (get_ssid(fd, if_name, essid))
		snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c %s",
			 0xef, 0x87, 0xab, ip);
	else
		snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c %.*s - %s", 0xef, 0x87,
			 0xab, MAX_BLOCK_SIZE - INET_ADDRSTRLEN - 8, essid, ip);

exit:
	close(fd);
	return ret;
}
