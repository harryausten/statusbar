#![doc = include_str!("../README.md")]
#![warn(
    clippy::all,
    clippy::cargo,
    clippy::nursery,
    clippy::pedantic,
    // Individually enabled lints from clippy::restriction
    clippy::assertions_on_result_states,
    clippy::clone_on_ref_ptr,
    clippy::empty_structs_with_brackets,
    clippy::exit,
    clippy::if_then_some_else_none,
    clippy::impl_trait_in_params,
    clippy::lossy_float_literal,
    clippy::map_err_ignore,
    clippy::missing_docs_in_private_items,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::str_to_string,
    clippy::string_to_string,
    clippy::try_err,
    clippy::undocumented_unsafe_blocks,
    clippy::unnecessary_safety_comment,
    clippy::unneeded_field_pattern,
    clippy::unwrap_used
)]
// TODO: Multiple windows crate versions being pulled in
#![allow(clippy::multiple_crate_versions)]

use std::time::{Duration, Instant};

use anyhow::{bail, Error};
use tokio::{join, sync::mpsc, task::JoinHandle, time};

#[cfg(feature = "backlight")]
use statusbar::backlight::Backlight;
#[cfg(feature = "battery")]
use statusbar::battery::Battery;
#[cfg(feature = "cpu")]
use statusbar::cpu::Cpu;
#[cfg(feature = "date")]
use statusbar::date::Date;
#[cfg(feature = "disk")]
use statusbar::disk::Disk;
#[cfg(feature = "media")]
use statusbar::media::Media;
#[cfg(feature = "network")]
use statusbar::network::Network;
#[cfg(feature = "packages")]
use statusbar::packages::Packages;
#[cfg(feature = "ram")]
use statusbar::ram::Ram;
#[cfg(feature = "temp")]
use statusbar::temp::Temp;
#[cfg(feature = "volume")]
use statusbar::volume::Volume;
use statusbar::Block;

/// Function to calculate the number of enabled blocks at compiletime
const fn num_blocks() -> usize {
    let mut num_blocks = 0;
    num_blocks += if cfg!(feature = "backlight") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "battery") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "cpu") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "date") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "disk") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "media") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "network") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "packages") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "ram") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "temp") { 1 } else { 0 };
    num_blocks += if cfg!(feature = "volume") { 1 } else { 0 };
    num_blocks
}

/// Number of enabled blocks
const NUM_BLOCKS: usize = num_blocks();

/// Iterate over all blocks, running any that need to be updated
///
/// Takes current time and slice of all block data as inputs. Returns the combined statusbar string
/// of all block content, to be displayed.
fn run_blocks(blocks: &mut [BlockData]) -> String {
    blocks
        .iter_mut()
        .map(|data| {
            if let Ok(s) = data.rx.try_recv() {
                data.text = s;
            }

            data.text.as_str()
        })
        .filter(|block| !block.is_empty())
        .collect::<Vec<&str>>()
        .join(" | ")
}

/// Struct to hold all data realting to a single statusbar block
struct BlockData {
    /// Handle for the task running this block
    _handle: JoinHandle<()>,
    /// Receiver for block updates
    rx: mpsc::Receiver<String>,
    /// The currently stored content of the block
    text: String,
}

impl BlockData {
    /// Helper function for creating a block data instance
    fn new<F, T>(block_factory: F, frequency_s: u64) -> Self
    where
        F: Fn() -> T + Send + Sync + 'static,
        T: Block + Send + 'static,
    {
        let (tx, rx) = mpsc::channel(1);
        let handle = tokio::spawn(async move {
            let mut now = Instant::now();
            let mut block = block_factory();

            loop {
                now += Duration::from_secs(frequency_s);
                join!(
                    tx.send(block.run().await.unwrap_or_default()),
                    time::sleep(now - Instant::now()),
                )
                .0
                .expect("Channel closed");
            }
        });

        Self {
            _handle: handle,
            rx,
            text: String::new(),
        }
    }
}

/// Main entrypoint to the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    if NUM_BLOCKS == 0 {
        bail!("No blocks enabled");
    }

    let mut blocks: [BlockData; NUM_BLOCKS] = [
        #[cfg(feature = "disk")]
        BlockData::new(Disk::default, 5),
        #[cfg(feature = "cpu")]
        BlockData::new(Cpu::default, 2),
        #[cfg(feature = "temp")]
        BlockData::new(Temp::default, 1),
        #[cfg(feature = "ram")]
        BlockData::new(Ram::default, 2),
        #[cfg(feature = "volume")]
        BlockData::new(Volume::default, 10),
        #[cfg(feature = "backlight")]
        BlockData::new(Backlight::default, 10),
        #[cfg(feature = "packages")]
        BlockData::new(
            || Packages::new().expect("Failed to create Packages block"),
            10,
        ),
        #[cfg(feature = "network")]
        BlockData::new(Network::default, 5),
        #[cfg(feature = "media")]
        BlockData::new(|| Media::new().expect("Failed to create Media block"), 10),
        #[cfg(feature = "battery")]
        BlockData::new(Battery::default, 5),
        #[cfg(feature = "date")]
        BlockData::new(Date::default, 1),
    ];
    let mut now = Instant::now();

    loop {
        now += Duration::from_secs(1);
        let fut = time::sleep(now - Instant::now());
        println!("{}", run_blocks(&mut blocks));
        fut.await;
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use super::main;
    use tokio::{task, time};

    // TODO: How to stop this from hanging?
    #[ignore]
    #[tokio::test]
    async fn test_main() {
        let child = task::spawn_blocking(main);

        time::sleep(Duration::from_secs(2)).await;

        assert!(!child.is_finished());
    }
}
