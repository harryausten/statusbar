// SPDX-License-Identifier: MIT
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <threads.h>

#include "bar.h"
#include "utils.h"

#define NUM_BLOCKS ARRAY_SIZE(bar)

static const long ns_in_sec = 1000000000;
static const long refresh_interval_ns = ns_in_sec / 10;
static bool signal_received[NUM_BLOCKS];

struct block_data {
	cnd_t cond;
	mtx_t mutex;
	thrd_t thread;
	struct block *block;
	char buf[MAX_BLOCK_SIZE];
	bool grabbed;
	time_t last_run;
};

static int run_block(void *vargp)
{
	struct block_data *d = vargp;
	struct block *b = d->block;

	while (true) {
		if (cnd_wait(&d->cond, &d->mutex) == thrd_error)
			return -1;

		if (b->exec(b->buf))
			b->buf[0] = '\0';
	}

	return 0;
}

static void handle_signal(int sig)
{
	if (sig < SIGRTMIN + 2)
		return;

	if (sig > SIGRTMAX)
		return;

	signal_received[sig - SIGRTMIN - 2] = true;
}

static int initialise_blocks(struct block_data data[NUM_BLOCKS])
{
	for (size_t i = 0; i < NUM_BLOCKS; ++i) {
		struct block_data *d = &data[i];
		struct block *b = &bar[i];

		d->buf[0] = '\0';
		d->block = b;
		d->grabbed = true;
		d->last_run = 0;

		if (cnd_init(&d->cond) != thrd_success)
			return -1;

		if (mtx_init(&d->mutex, mtx_plain) != thrd_success)
			return -1;

		if (thrd_create(&d->thread, run_block, d) != thrd_success)
			return -1;

		if (b->signal != -1) {
			struct sigaction sa = {
				.sa_handler = handle_signal,
				.sa_flags = 0,
			};

			sigemptyset(&sa.sa_mask);
			signal_received[b->signal] = false;
			sigaction(SIGRTMIN + 2 + b->signal, &sa, NULL);
		}
	}

	return 0;
}

static bool block_needs_running(struct block_data *d, const time_t now)
{
	struct block *b = d->block;
	const time_t next_run = d->last_run + (time_t)b->freq_s;

	if (next_run <= now)
		return true;

	if ((b->signal == -1) || !signal_received[b->signal])
		return false;

	signal_received[b->signal] = false;
	return true;
}

static int process_blocks(struct block_data data[NUM_BLOCKS], const time_t now)
{
	for (size_t i = 0; i < NUM_BLOCKS; ++i) {
		struct block_data *d = &data[i];
		const struct block *b = d->block;

		if (mtx_trylock(&d->mutex) != thrd_success)
			continue;

		if (!d->grabbed) {
			strncpy(d->buf, b->buf, MAX_BLOCK_SIZE);
			d->grabbed = true;
		}

		mtx_unlock(&d->mutex);

		if (block_needs_running(d, now)) {
			d->grabbed = false;
			cnd_signal(&d->cond);
			d->last_run = now;
		}
	}

	return 0;
}

static size_t get_final_block(struct block_data data[NUM_BLOCKS])
{
	size_t final = 0;

	for (size_t i = 0; i < NUM_BLOCKS; ++i)
		if (data[i].buf[0] != '\0')
			final = i;

	return final;
}

static void print_blocks(struct block_data data[NUM_BLOCKS])
{
	const size_t final = get_final_block(data);

	for (size_t i = 0; i < NUM_BLOCKS; ++i) {
		if (data[i].buf[0] == '\0')
			continue;

		printf("%s", data[i].buf);

		if (i != final)
			fputs(" | ", stdout);
	}

	fflush(stdout);
}

static void timespec_next(struct timespec *t)
{
	const long nsecs = t->tv_nsec + refresh_interval_ns;

	t->tv_sec += nsecs / ns_in_sec;
	t->tv_nsec = nsecs % ns_in_sec;
}

int main(void)
{
	struct block_data data[NUM_BLOCKS];
	struct timespec now;
	int ret;

	ret = initialise_blocks(data);
	if (ret)
		return ret;

	ret = clock_gettime(CLOCK_REALTIME, &now);
	if (ret)
		return -errno;

	for (bool first_time = true;;) {
		ret = process_blocks(data, now.tv_sec);
		if (ret)
			return ret;

		if (first_time)
			first_time = false;
		else
			putchar('\n');

		print_blocks(data);
		timespec_next(&now);

		ret = timespec_sleep_until(now);
		if (ret)
			return ret;
	}

	return 0;
}
