// SPDX-License-Identifier: MIT
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "block.h"

int ram_exec(char *buf)
{
	FILE *fp;
	char line[100];
	size_t total = 0;
	size_t free = 0;
	size_t slab_reclaimable = 0;
	size_t cached = 0;
	size_t buffers = 0;
	size_t used = 0;
	int ret;

	fp = fopen("/proc/meminfo", "r");
	if (!fp)
		return -errno;

	while (fgets(line, 100, fp)) {
		char *head = line;
		char *tail = strchr(head, ':');

		if (!strncmp(line, "MemTotal", tail - head))
			total = strtoul(tail+1, NULL, 10);
		else if (!strncmp(line, "MemFree", tail - head))
			free = strtoul(tail+1, NULL, 10);
		else if (!strncmp(line, "SReclaimable", tail - head))
			slab_reclaimable = strtoul(tail+1, NULL, 10);
		else if (!strncmp(line, "Cached", tail - head))
			cached = strtoul(tail+1, NULL, 10);
		else if (!strncmp(line, "Buffers", tail - head))
			buffers = strtoul(tail+1, NULL, 10);
	}

	ret = fclose(fp);
	if (ret)
		return -errno;

	// This is the same as how it is calculated in procps for the free program:
	// https://gitlab.com/procps-ng/procps/-/blob/master/proc/sysinfo.c#L789
	used = total - free - slab_reclaimable - cached - buffers;
	ret = snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %.1fG/%.1fG", 0xf0, 0x9f, 0x90, 0x8f,
		       (double)(used)/(1<<20), (double)(total)/(1<<20));

	if (ret > 0)
		return 0;

	return ret;
}
