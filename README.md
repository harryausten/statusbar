# statusbar

[![pipeline status](https://gitlab.com/harryausten/statusbar/badges/master/pipeline.svg)](https://gitlab.com/harryausten/statusbar/-/commits/master)
[![coverage report](https://gitlab.com/harryausten/statusbar/badges/master/coverage.svg)](https://gitlab.com/harryausten/statusbar/-/commits/master)

Personal commandline statusbar project, intended for being displayed in the top panel of a window manager. For example,
see my
[config](https://gitlab.com/harryausten/dotfiles/-/blob/7f1a4abc49214f7e72e7482f0d13840838362e92/.config/sway/config#L26)
for [`swaywm`](https://swaywm.org).

## Example Output

```text
/: 685G/931G | 💻 2% | 🔥 49°C | 🐏 3G/15G | 📦 0 |  192.168.1.203 | ▶ Asahi Linux: USB haunts me | 📆 Fri 16 Jun 17:43:35
```
