//! Implementation details for block showing current battery charge percentage

use anyhow::{anyhow, Context};
use async_trait::async_trait;

use super::Block;

/// Battery block private data
#[derive(Default)]
pub struct Battery;

#[async_trait]
impl Block for Battery {
    /// Check the current charge of the battery
    ///
    /// # Errors
    ///
    /// Fails if unable to find a battery device, or unable to read or parse the `capacity` or
    /// `status` attributes of the detected device.
    async fn run(&mut self) -> anyhow::Result<String> {
        let mut dir = tokio::fs::read_dir("/sys/class/power_supply")
            .await
            .context("read_dir sysfs power_supply")?;

        loop {
            let entry = dir
                .next_entry()
                .await
                .context("Failed to parse power_supply sysfs dir")?
                .ok_or_else(|| anyhow!("No battery devices"))?;

            let mut path = entry.path();
            path.push("type");

            if tokio::fs::read_to_string(path)
                .await
                .context("read type prop")?
                .trim_end()
                != "Battery"
            {
                continue;
            }

            path = entry.path();
            path.push("capacity");
            let capacity: usize = tokio::fs::read_to_string(path)
                .await
                .context("read capacity prop")?
                .trim_end()
                .parse()
                .context("parse capacity prop")?;

            path = entry.path();
            path.push("status");
            let status = tokio::fs::read_to_string(path)
                .await
                .context("read status prop")?;

            let icon = match status.trim_end() {
                "Charging" => "⬆",
                _ => "⬇",
            };

            return Ok(format!("{icon} {capacity}%"));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Battery, Block};

    #[test]
    const fn test_new() {
        let _ = Battery;
    }

    #[tokio::test]
    async fn test_run() {
        // TODO: Is there a way to fully test run() in CI? Emulated udev devices?
        let _ = Battery.run().await;
    }
}
