// SPDX-License-Identifier: MIT
#include <errno.h>
#include <stdio.h>
#include <sys/statvfs.h>

#include "block.h"

static const char fs_location[] = "/";
static const size_t MiB = 1U << 20;
static const size_t KiB = 1U << 10;

static void disk_human_readable(size_t *x, char *c)
{
	if (*x > MiB) {
		*x = *x / MiB;
		*c = 'G';
	} else if (*x > KiB) {
		*x = *x / KiB;
		*c = 'M';
	} else {
		*c = 'K';
	}
}

static int get_sizes(size_t *total, size_t *used)
{
	struct statvfs stats;
	size_t block_size;

	if (statvfs(fs_location, &stats))
		return -errno;

	block_size = stats.f_frsize / KiB;
	*total = block_size * stats.f_blocks;
	*used = *total - block_size * stats.f_bfree;

	return 0;
}

int disk_exec(char *buf)
{
	int ret;
	size_t total = 0;
	size_t used = 0;
	char total_unit;
	char used_unit;

	ret = get_sizes(&total, &used);
	if (ret)
		return ret;

	disk_human_readable(&used, &used_unit);
	disk_human_readable(&total, &total_unit);
	ret = snprintf(buf, MAX_BLOCK_SIZE, "%s: %zu%c/%zu%c", fs_location,
			used, used_unit, total, total_unit);

	if (ret < 0)
		return ret;

	return 0;
}
