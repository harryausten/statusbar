// SPDX-License-Identifier: MIT
#define _GNU_SOURCE
#include <errno.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "block.h"
#include "utils.h"

static const struct timespec interval = {
	.tv_sec = 1,
	.tv_nsec = 0,
};

static size_t get_num_cpus(void)
{
	cpu_set_t set;

	if (sched_getaffinity(getpid(), sizeof(set), &set))
		return 0;

	return CPU_COUNT(&set);
}

static int get_idle(size_t *idle)
{
	FILE *fp = fopen("/proc/stat", "r");

	if (!fp)
		return -errno;

	if (fscanf(fp, "cpu %*u %*u %*u %zu", idle) != 1) {
		fclose(fp);
		return -errno;
	}

	if (fclose(fp))
		return -errno;

	return 0;
}

int cpu_exec(char *buf)
{
	size_t idle[2];
	size_t num_cpus;
	uint8_t percentage;
	int ret;

	num_cpus = get_num_cpus();
	if (!num_cpus)
		return -errno;

	ret = get_idle(&idle[0]);
	if (ret)
		return ret;

	timespec_sleep(interval);
	ret = get_idle(&idle[1]);
	if (ret)
		return ret;

	percentage = 100 - (double)(idle[1] - idle[0]) / num_cpus;
	ret = snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %u%%",
			0xf0, 0x9f, 0x92, 0xbb, percentage);
	if (ret < 0)
		return ret;

	return 0;
}
