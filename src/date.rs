//! Implementation details for block showing current timestamp

use anyhow::Error;
use async_trait::async_trait;
use chrono::Local;

use super::Block;

/// Date block private data
#[derive(Default)]
pub struct Date;

#[async_trait]
impl Block for Date {
    /// Get the current date and time
    ///
    /// # Errors
    ///
    /// Doesn't return any errors
    async fn run(&mut self) -> Result<String, Error> {
        Ok(format!("📆 {}", Local::now().format("%a %d %b %T")))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Date};

    #[tokio::test]
    async fn test_run() {
        Date.run().await.expect("Failed to query current timestamp");
    }
}
