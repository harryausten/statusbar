// SPDX-License-Identifier: MIT
/*
 * CPU temperature block
 *
 * Copyright (c) 2020 Harry Austen
 */
#include "block.h"

#include <errno.h>
#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct udev *udev;
static struct udev_enumerate *enumerate;

static int temp_setup(void)
{
	int ret;

	udev = udev_new();
	if (!udev)
		return -errno;

	enumerate = udev_enumerate_new(udev);
	if (!enumerate)
		return -errno;

	ret = udev_enumerate_add_match_subsystem(enumerate, "hwmon");
	if (ret)
		return ret;

	ret = udev_enumerate_scan_devices(enumerate);
	return ret;
}

static int temp_read(char *buf)
{
	int ret = -ENODEV;
	struct udev_list_entry *devices;
	struct udev_list_entry *entry;

	devices = udev_enumerate_get_list_entry(enumerate);

	udev_list_entry_foreach(entry, devices) {
		const char *path;
		const char *name_str;
		const char *temp_str;
		struct udev_device *device;

		path = udev_list_entry_get_name(entry);
		if (!path)
			continue;

		device = udev_device_new_from_syspath(udev, path);
		if (!device)
			continue;

		name_str = udev_device_get_sysattr_value(device, "name");
		if (!name_str || (strcmp(name_str, "cpu") != 0 && strcmp(name_str, "k10temp") != 0 && strcmp(name_str, "coretemp") != 0))
			goto unref_dev;

		temp_str = udev_device_get_sysattr_value(device, "temp1_input");
		if (temp_str) {
			const size_t temp = strtoul(temp_str, NULL, 10) / 1000;

			snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %zu°C", 0xf0, 0x9f, 0x94, 0xa5, temp);
			ret = 0;
		}

unref_dev:
		udev_device_unref(device);
		if (!ret)
			break;
	}

	return ret;
}

int temp_exec(char *buf)
{
	int ret;

	ret = temp_setup();
	if (ret)
		goto temp_error;

	ret = temp_read(buf);

temp_error:
	if (enumerate)
		udev_enumerate_unref(enumerate);
	if (udev)
		udev_unref(udev);

	return ret;
}
