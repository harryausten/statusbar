//! Implementation details for block showing current CPU utilisation percentage

use anyhow::Error;
use async_trait::async_trait;
use sysinfo::{CpuRefreshKind, System};

use super::Block;

/// CPU block private data
#[derive(Default)]
pub struct Cpu {
    /// Access to the underlying [`sysinfo`] library for querying CPU information
    system: System,
}

#[async_trait]
impl Block for Cpu {
    /// Check the current CPU utilisation percentage
    ///
    /// # Errors
    ///
    /// Doesn't return any errors
    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    async fn run(&mut self) -> Result<String, Error> {
        self.system
            .refresh_cpu_specifics(CpuRefreshKind::everything());

        Ok(format!(
            "💻 {}%",
            self.system.global_cpu_info().cpu_usage().round() as u8
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Cpu};

    #[test]
    fn test_new() {
        Cpu::default();
    }

    #[tokio::test]
    async fn test_run() {
        Cpu::default()
            .run()
            .await
            .expect("Failed to check CPU usage");
    }
}
