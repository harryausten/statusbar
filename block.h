/* SPDX-License-Identifier: MIT */
#ifndef __BLOCK_H__
#define __BLOCK_H__

#include <errno.h>
#include <time.h>

#define MAX_BLOCK_SIZE 50U

struct block {
	char buf[MAX_BLOCK_SIZE];
	int (* const exec)(char *buf);
	const size_t freq_s;
	const int signal;
};

#ifdef HAVE_UDEV_LIB
int backlight_exec(char *buf);
int battery_exec(char *buf);
int temp_exec(char *buf);
#else
static inline int backlight_exec(__attribute__((unused)) char *buf)
{
	return -ENODEV;
}

static inline int battery_exec(__attribute__((unused)) char *buf)
{
	return -ENODEV;
}

static inline int temp_exec(__attribute__((unused)) char *buf)
{
	return -ENODEV;
}
#endif

#ifdef HAVE_ALPM_LIB
int packages_exec(char *buf);
#else
static inline int packages_exec(__attribute__((unused)) char *buf)
{
	return -ENODEV;
}
#endif

#ifdef HAVE_PULSE_LIB
int volume_exec(char *buf);
#else
static inline int volume_exec(__attribute__((unused)) char *buf)
{
	return -ENODEV;
}
#endif

int cpu_exec(char *buf);
int date_exec(char *buf);
int disk_exec(char *buf);
int network_exec(char *buf);
int ram_exec(char *buf);

#endif // __BLOCK_H__
