/* SPDX-License-Identifier: MIT */
#ifndef __UTILS_H__
#define __UTILS_H__

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

struct timespec;

int timespec_sleep(struct timespec time);
int timespec_sleep_until(const struct timespec until);

#endif // __UTILS_H__
