#![doc = include_str!("../README.md")]
#![warn(
    clippy::all,
    clippy::cargo,
    clippy::nursery,
    clippy::pedantic,
    // Individually enabled lints from clippy::restriction
    clippy::assertions_on_result_states,
    clippy::clone_on_ref_ptr,
    clippy::empty_structs_with_brackets,
    clippy::exit,
    clippy::if_then_some_else_none,
    clippy::impl_trait_in_params,
    clippy::lossy_float_literal,
    clippy::map_err_ignore,
    clippy::missing_docs_in_private_items,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::str_to_string,
    clippy::string_to_string,
    clippy::try_err,
    clippy::undocumented_unsafe_blocks,
    clippy::unnecessary_safety_comment,
    clippy::unneeded_field_pattern,
    clippy::unwrap_used
)]
// TODO: Multiple windows crate versions being pulled in
#![allow(clippy::multiple_crate_versions)]

#[cfg(feature = "backlight")]
pub mod backlight;
#[cfg(feature = "battery")]
pub mod battery;
#[cfg(feature = "cpu")]
pub mod cpu;
#[cfg(feature = "date")]
pub mod date;
#[cfg(feature = "disk")]
pub mod disk;
#[cfg(feature = "media")]
pub mod media;
#[cfg(feature = "network")]
pub mod network;
#[cfg(feature = "packages")]
pub mod packages;
#[cfg(feature = "ram")]
pub mod ram;
#[cfg(feature = "temp")]
pub mod temp;
#[cfg(feature = "volume")]
pub mod volume;

use anyhow::Error;
use async_trait::async_trait;
use tokio::signal::unix::SignalKind;

/// Simple single function trait that all statusbar block types should implement
#[async_trait]
pub trait Block {
    /// Function that gets run whenever the program wants to perform a block update
    ///
    /// Returns the intended contains of the block as a [`String`]
    ///
    /// # Errors
    ///
    /// Can error for any number of different reasons, dependent on the type of block. When an
    /// error occurs, the program should either use the *last successful string*, or set the block
    /// text to an empty string.
    async fn run(&mut self) -> Result<String, Error>;
}

/// Multiplication constant for kibibytes (KiB)
const K: u64 = 1 << 10;
/// Multiplication constant for mebibytes (MiB)
const M: u64 = 1 << 20;
/// Multiplication constant for gibibytes (GiB)
const G: u64 = 1 << 30;

/// Convert the provided integer number of bytes to a human readable string with a binary prefix
/// character
#[must_use]
pub(crate) fn human_readable(bytes: u64) -> String {
    if bytes > G {
        format!("{}G", bytes.checked_div(G).expect("G is not zero"))
    } else if bytes > M {
        format!("{}M", bytes.checked_div(M).expect("M is not zero"))
    } else if bytes > K {
        format!("{}K", bytes.checked_div(K).expect("K is not zero"))
    } else {
        bytes.to_string()
    }
}

/// Create a tokio representation of a POSIX.1b real-time signal
///
/// # Errors
///
/// If input is larger than number of real-time signals supported on the system
pub fn rtsignal(i: u16) -> anyhow::Result<SignalKind> {
    let rtmin = libc::SIGRTMIN();
    let rtmax = libc::SIGRTMAX();
    let sig = rtmin + i32::from(i);
    anyhow::ensure!(sig <= rtmax, "SIGRTMIN + {} > SIGRTMAX", i);
    Ok(SignalKind::from_raw(sig))
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use super::{human_readable, rtsignal, SignalKind};

    #[test]
    fn test_gibibytes() {
        const TWO_GIBIBYTES: u64 = 2 << 30;
        assert_eq!(human_readable(TWO_GIBIBYTES), "2G");
    }

    #[test]
    fn test_mebibytes() {
        const FOUR_MEBIBYTES: u64 = 4 << 20;
        assert_eq!(human_readable(FOUR_MEBIBYTES), "4M");
    }

    #[test]
    fn test_kibibytes() {
        const SIX_KIBIBYTES: u64 = 6 << 10;
        assert_eq!(human_readable(SIX_KIBIBYTES), "6K");
    }

    #[test]
    fn test_bytes() {
        const EIGHT_BYTES: u64 = 8;
        assert_eq!(human_readable(EIGHT_BYTES), "8");
    }

    #[test]
    fn test_valid_rtsig() {
        assert_eq!(rtsignal(0).unwrap(), SignalKind::from_raw(libc::SIGRTMIN()));
    }

    #[test]
    fn test_invalid_rtsig() {
        let i = (libc::SIGRTMAX() + 1).try_into().unwrap();
        rtsignal(i).unwrap_err();
    }
}
