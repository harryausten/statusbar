/* SPDX-License-Identifier: MIT */
#ifndef __BAR_H__
#define __BAR_H__

#include "block.h"

static struct block bar[] = {
	{
		.exec = disk_exec,
		.freq_s = 5,
		.signal = -1,
	},
	{
		.exec = cpu_exec,
		.freq_s = 2,
		.signal = -1,
	},
	{
		.exec = temp_exec,
		.freq_s = 1,
		.signal = -1,
	},
	{
		.exec = ram_exec,
		.freq_s = 2,
		.signal = -1,
	},
	{
		.exec = volume_exec,
		.freq_s = 10,
		.signal = 1,
	},
	{
		.exec = backlight_exec,
		.freq_s = 10,
		.signal = 0,
	},
	{
		.exec = packages_exec,
		.freq_s = 10,
		.signal = 2,
	},
	{
		.exec = network_exec,
		.freq_s = 5,
		.signal = -1,
	},
	{
		.exec = battery_exec,
		.freq_s = 1,
		.signal = -1,
	},
	{
		.exec = date_exec,
		.freq_s = 1,
		.signal = -1,
	},
};

#endif // __BAR_H__
