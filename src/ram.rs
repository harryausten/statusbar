//! Implementation details for block showing RAM utilisation

use anyhow::Error;
use async_trait::async_trait;
use sysinfo::System;

use super::{human_readable, Block};

/// RAM block private data
#[derive(Default)]
pub struct Ram {
    /// Access to the underlying [`sysinfo`] library for querying RAM utilisation
    system: System,
}

#[async_trait]
impl Block for Ram {
    /// Check current RAM utilisation
    ///
    /// # Errors
    ///
    /// Doesn't return any errors
    async fn run(&mut self) -> Result<String, Error> {
        self.system.refresh_memory();

        Ok(format!(
            "🐏 {}/{}",
            human_readable(self.system.used_memory()),
            human_readable(self.system.total_memory())
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Ram};

    #[test]
    fn test_new() {
        Ram::default();
    }

    #[tokio::test]
    async fn test_run() {
        Ram::default()
            .run()
            .await
            .expect("Failed to query RAM utilitisation");
    }
}
