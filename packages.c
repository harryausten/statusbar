// SPDX-License-Identifier: MIT
#include <alpm.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <string.h>

#include "block.h"

static const char root[] = "/";
static const char dbpath[] = "/var/lib/pacman/";
static const char configfile[] = "/etc/pacman.conf";
static const char syncfile[] = "/.cache/update/syncing";

int add_repos(alpm_handle_t *handle)
{
	int ret = 0;
	FILE *fp;
	char line[PATH_MAX + 1];

	fp = fopen(configfile, "r");
	if (!fp)
		return -errno;

	while (fgets(line, PATH_MAX, fp)) {
		char *repo = &line[1];

		if (line[0] != '[')
			continue;

		for (char *c = repo; c; ++c)
			if (*c == ']') {
				*c = '\0';
				break;
			}

		if (!strncmp(repo, "options", PATH_MAX))
			continue;

		if (!alpm_register_syncdb(handle, repo, ALPM_SIG_USE_DEFAULT)) {
			printf("couldn't register %s database: %s\n",
				repo, alpm_strerror(alpm_errno(handle)));
			ret = -1;
			goto close_config;
		}
	}

close_config:
	fclose(fp);
	return ret;
}

alpm_handle_t *setup_alpm(void)
{
	alpm_errno_t err;
	alpm_list_t *sync_dbs;
	alpm_handle_t *handle;

	handle = alpm_initialize(root, dbpath, &err);
	if (!handle)
		goto alpm_setup_failed;

	if (add_repos(handle))
		goto alpm_setup_failed;

	sync_dbs = alpm_get_syncdbs(handle);
	if (!sync_dbs)
		goto alpm_setup_failed;

	for (alpm_list_t *i = sync_dbs; i; i = alpm_list_next(i))
		if (alpm_db_get_valid(i->data))
			goto alpm_setup_failed;

	return handle;

alpm_setup_failed:
	alpm_release(handle);
	return NULL;
}

static inline bool pkg_has_update(alpm_pkg_t *pkg, alpm_handle_t *handle)
{
	return alpm_sync_get_new_version(pkg, alpm_get_syncdbs(handle));
}

size_t num_packages(alpm_handle_t *handle, alpm_db_t *db_local)
{
	size_t packages = 0;
	alpm_list_t *pkgcache = alpm_db_get_pkgcache(db_local);

	for (alpm_list_t *i = pkgcache; i; i = alpm_list_next(i))
		if (pkg_has_update(i->data, handle))
			packages++;

	return packages;
}

int packages_exec(char *buf)
{
	const char * const home = getenv("HOME");
	char syncfile_path[PATH_MAX + 1];
	int ret;
	alpm_handle_t *handle;
	alpm_db_t *db_local;
	size_t packages;

	handle = setup_alpm();
	if (!handle) {
		ret = -1;
		goto exit;
	}

	db_local = alpm_get_localdb(handle);
	if (!db_local) {
		ret = -1;
		goto exit;
	}

	packages = num_packages(handle, db_local);
	ret = 0;
	strncpy(syncfile_path, home, PATH_MAX);
	strncat(syncfile_path, syncfile, PATH_MAX - strlen(syncfile_path));

	if (access(syncfile_path, F_OK))
		snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %zu", 0xf0, 0x9f, 0x93,
			 0xa6, packages);
	else
		snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %zu", 0xf0, 0x9f, 0x94,
			 0x83, packages);
exit:
	alpm_release(handle);
	return ret;
}
