//! Implementation details for block showing current IP address

use anyhow::Error;
use async_trait::async_trait;
use std::{
    io::{self, ErrorKind},
    net::{IpAddr, Ipv4Addr, SocketAddrV4},
};

use super::Block;

use futures::{future, TryFutureExt, TryStreamExt};
use rtnetlink::IpVersion;
use tokio::net::UdpSocket;
use wl_nl80211::Nl80211Attr;

/// Network block private data
#[derive(Default)]
pub struct Network;

/// Local socket address to bind to
const LOCAL_ADDR: SocketAddrV4 = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 0);
/// Remote socket address to connect to
const REMOTE_ADDR: SocketAddrV4 = SocketAddrV4::new(Ipv4Addr::new(1, 1, 1, 1), 80);

/// Determine the IP address for the default route
async fn get_default_ipaddr() -> io::Result<IpAddr> {
    let socket = UdpSocket::bind(LOCAL_ADDR).await?;

    socket.connect(REMOTE_ADDR).await?;

    Ok(socket.local_addr()?.ip())
}

/// Determine the index for the interface with the provided IP address
async fn get_interface_index(addr: &IpAddr) -> io::Result<u32> {
    let (connection, handle, _) = rtnetlink::new_connection()?;
    tokio::spawn(connection);

    let mut routes = handle.route().get(IpVersion::V4).execute();
    while let Ok(Some(route)) = routes.try_next().await {
        let mut ip = None;
        let mut oif = None;

        for attr in route.attributes {
            use netlink_packet_route::route::{RouteAddress, RouteAttribute};

            match attr {
                RouteAttribute::PrefSource(RouteAddress::Inet(addr)) => {
                    ip = Some(addr);
                }
                RouteAttribute::Oif(index) => oif = Some(index),
                _ => (),
            }
        }

        if let (Some(ip), Some(oif)) = (ip, oif) {
            if &ip == addr {
                return Ok(oif);
            }
        };
    }

    Err(io::Error::new(
        ErrorKind::NotFound,
        format!("No interface found for address: {addr}"),
    ))
}

/// Get the SSID for the interface with the provided index
async fn get_ssid_name(index: u32) -> io::Result<String> {
    let (connection, handle, _) = wl_nl80211::new_connection()?;
    tokio::spawn(connection);

    let mut ifaces = handle.interface().get().execute().await;
    while let Ok(Some(iface)) = ifaces.try_next().await {
        if !iface
            .payload
            .nlas
            .iter()
            .any(|nla| matches!(nla, Nl80211Attr::IfIndex(i) if i == &index))
        {
            continue;
        }

        for nla in iface.payload.nlas {
            if let Nl80211Attr::Ssid(ssid) = nla {
                return Ok(ssid);
            }
        }
    }

    Err(io::Error::new(ErrorKind::NotFound, "No SSID found"))
}

#[async_trait]
impl Block for Network {
    /// Update current network details
    ///
    /// # Errors
    ///
    /// Can fail to initialise tokio runtime or to determine default route
    async fn run(&mut self) -> Result<String, Error> {
        let addr = get_default_ipaddr().await?;

        (future::ready(Ok(&addr))
            .and_then(|addr| async move { get_interface_index(addr).await })
            .and_then(|index| async move { get_ssid_name(index).await })
            .await)
            .map_or_else(
                |_| Ok(format!(" {addr}")),
                |ssid| Ok(format!(" {ssid} - {addr}")),
            )
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Network};

    #[tokio::test]
    async fn test_run() {
        Network.run().await.expect("Failed to query IP address");
    }
}
