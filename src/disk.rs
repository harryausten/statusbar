//! Implementation details for block showing current disk storage utilisation

use anyhow::{anyhow, Error};
use async_trait::async_trait;
use sysinfo::Disks;

use super::{human_readable, Block};

/// Disk block private data
pub struct Disk<'a> {
    /// Absolute mountpoint path for usage checking
    mountpoint: &'a str,
    /// Access to the underlying [`sysinfo`] library for querying disk utilisation
    disks: Disks,
}

impl Disk<'_> {
    /// Create a new [`Disk`] instance
    #[must_use]
    pub fn new(mountpoint: &'static str) -> Self {
        Self {
            mountpoint,
            disks: Disks::new(),
        }
    }
}

impl Default for Disk<'_> {
    /// Create default [`Disk`] instance
    ///
    /// Uses root filesystem
    fn default() -> Self {
        Self::new("/")
    }
}

#[async_trait]
impl Block for Disk<'_> {
    /// Check current storage utilisation
    ///
    /// # Errors
    ///
    /// Fails if the configured mountpoint does not exist
    async fn run(&mut self) -> Result<String, Error> {
        let mountpoint = self.mountpoint;

        self.disks.refresh_list();

        let disk = self
            .disks
            .iter()
            .find(|disk| disk.mount_point().to_str() == Some(mountpoint))
            .ok_or_else(|| anyhow!("Failed to find mountpoint '{mountpoint}'"))?;

        let total = disk.total_space();
        let avail = disk.available_space();
        Ok(format!(
            "{}: {}/{}",
            mountpoint,
            human_readable(total.checked_sub(avail).ok_or_else(|| anyhow!(
                "Total space ({total}) should be greater than available space ({avail})"
            ))?),
            human_readable(disk.total_space())
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Disk};

    #[test]
    fn test_new() {
        Disk::default();
    }

    #[tokio::test]
    async fn test_run() {
        Disk::default()
            .run()
            .await
            .expect("Failed to check storage utilisation");
    }
}
