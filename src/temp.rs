//! Implementation details for block showing current CPU temperature

use anyhow::{anyhow, Context};
use async_trait::async_trait;

use super::Block;

/// Temp block private data
#[derive(Default)]
pub struct Temp;

#[async_trait]
impl Block for Temp {
    /// Check the current CPU temperature
    ///
    /// # Errors
    ///
    /// Fails if unable to find a sensor device, or unable to read or parse the `temp1_input`
    /// attribute of the detected device.
    async fn run(&mut self) -> anyhow::Result<String> {
        let mut dir = tokio::fs::read_dir("/sys/class/hwmon")
            .await
            .context("read_dir sysfs hwmon")?;
        loop {
            let entry = dir
                .next_entry()
                .await
                .context("Failed to parse hwmon sysfs dir")?
                .ok_or_else(|| anyhow!("No CPU temp devices"))?;

            let mut path = entry.path();
            path.push("name");

            let name = tokio::fs::read_to_string(path)
                .await
                .context("read name prop")?;
            match name.trim_end() {
                "cpu" | "k10temp" | "coretemp" => (),
                _ => continue,
            }

            path = entry.path();
            path.push("temp1_input");
            let val: isize = tokio::fs::read_to_string(path)
                .await
                .context("read temp1_input prop")?
                .trim_end()
                .parse()
                .context("parse temp1_input prop")?;

            return Ok(format!("🔥 {}°C", val / 1000));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Temp};

    #[test]
    const fn test_new() {
        let _ = Temp;
    }

    #[tokio::test]
    async fn test_run() {
        // TODO: Is there a way to fully test run() in CI? Emulated udev devices?
        let _ = Temp.run().await;
    }
}
