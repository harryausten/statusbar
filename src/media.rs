//! Implementation details for block showing info of currently playing media

use anyhow::Error;
use async_trait::async_trait;
use mpris::{PlaybackStatus, PlayerFinder};

use super::Block;

/// Media block private data
///
/// TODO: [`mpris::PlayerFinder`] is not Send, therefore `media` feature cannot be used currently.
/// Find workaround
pub struct Media {
    /// Access to the underlying [`mpris`] library for querying media playback details
    finder: PlayerFinder,
}

impl Media {
    /// Create a new [`Media`] instance
    ///
    /// # Errors
    ///
    /// Fails if D-Bus connection creation fails
    pub fn new() -> Result<Self, Error> {
        Ok(Self {
            finder: PlayerFinder::new()?,
        })
    }
}

#[async_trait]
impl Block for Media {
    /// Update current media playback details
    ///
    /// # Errors
    ///
    /// Fails if no active media player is found, no media is playing, no media title is found or
    /// if D-Bus returns an error
    async fn run(&mut self) -> Result<String, Error> {
        let player = self.finder.find_active()?;
        // TODO: Do I want to print player.identity()?

        let status = player.get_playback_status()?;
        let icon = match status {
            PlaybackStatus::Playing => '▶',
            PlaybackStatus::Paused => '⏸',
            PlaybackStatus::Stopped => anyhow::bail!("No media playing"),
        };

        let metadata = player.get_metadata()?;
        let title = metadata.title().ok_or(anyhow::anyhow!("No title found"))?;

        // TODO: Do I want to print artists?
        /*
        let artists = metadata.artists();
        if let Some(artists) = artists {
            dbg!(artists);
        }
        */

        Ok(format!("{icon} {title}"))
    }
}

#[cfg(test)]
mod tests {
    use super::{Block, Media};

    #[test]
    // TODO: Find a way to workaround the following error in CI (headless environment):
    // TransportError(D-Bus error: Unable to autolaunch a dbus-daemon without a $DISPLAY for X11
    // (org.freedesktop.DBus.Error.NotSupported))
    #[ignore]
    fn test_new() {
        Media::new().expect("Failed to create Media block");
    }

    #[tokio::test]
    #[ignore]
    fn test_run() {
        // TODO: Is there a way to fully test run() in CI? Run up a media player?
        let _ = Media::new()
            .expect("Failed to create Media block")
            .run()
            .await;
    }
}
