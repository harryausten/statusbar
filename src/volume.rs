//! Implementation details for block showing system audio volume

use std::{cell::RefCell, rc::Rc};

use anyhow::Error;
use async_trait::async_trait;
use wireplumber::{
    core::Core,
    lib::glib::{object::ObjectExt, Variant, VariantDict, VariantTy},
    plugin::{ComponentLoader, Plugin, PluginFeatures},
    pw::{Properties, PW_KEY_APP_NAME},
};

use super::Block;

/// Pipewire context callback
///
/// # Errors
///
/// Fails if any if the attempted pipewire/wireplumber functions fail
async fn async_run(core: &Core) -> Result<(bool, f64), Error> {
    core.load_component(
        "libwireplumber-module-default-nodes-api",
        ComponentLoader::TYPE_WIREPLUMBER_MODULE,
        None,
    )?;
    core.load_component(
        "libwireplumber-module-mixer-api",
        ComponentLoader::TYPE_WIREPLUMBER_MODULE,
        None,
    )?;
    core.connect_future().await?;

    let mixer_api = Plugin::find(core, "mixer-api").expect("Failed to find mixer-api plugin");
    let default_nodes_api =
        Plugin::find(core, "default-nodes-api").expect("Failed to find default-nodes-api plugin");

    mixer_api.activate_future(PluginFeatures::ENABLED).await?;
    default_nodes_api
        .activate_future(PluginFeatures::ENABLED)
        .await?;

    let id: u32 = default_nodes_api.emit_by_name("get-default-node", &[&"Audio/Sink"]);
    let dict = VariantDict::new(Some(
        &mixer_api.emit_by_name::<Variant>("get-volume", &[&id]),
    ));

    let mute = dict
        .lookup_value("mute", Some(VariantTy::BOOLEAN))
        .ok_or("mute property should be returned by mixer api")?
        .try_get::<bool>()?;
    let volume = dict
        .lookup_value("volume", Some(VariantTy::DOUBLE))
        .ok_or("volume property should be returned by mixer api")?
        .try_get::<f64>()?;

    Ok((mute, volume))
}

/// Volume block private data
pub struct Volume;

impl Default for Volume {
    /// Create a new [`Volume`] instance
    fn default() -> Self {
        Core::init();
        Self
    }
}

#[async_trait]
impl Block for Volume {
    /// Check the current volume percentage
    ///
    /// # Errors
    ///
    /// Fails if pipewire connection or interaction fails
    async fn run(&mut self) -> Result<String, Error> {
        let props = Properties::new();
        props.insert(PW_KEY_APP_NAME, "statusbar");

        let main_res = Rc::new(RefCell::new(None));
        Core::run(Some(props), |context, mainloop, core| {
            let main_res = Rc::<RefCell<Option<(bool, f64)>>>::clone(&main_res);
            context.spawn_local(async move {
                if let Ok(res) = async_run(&core).await {
                    *main_res.borrow_mut() = Some(res);
                }

                mainloop.quit();
            });
        });

        let main_res = main_res.borrow_mut().take();
        if let Some((mute, volume)) = main_res {
            let icon = if mute { '🔇' } else { '🔊' };
            #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
            let volume = (volume * 100.0).round() as u8;

            Ok(format!("{icon} {volume}%"))
        } else {
            Err("pipewire connection failed".into())
        }
    }
}

#[cfg(test)]
#[allow(clippy::default_constructed_unit_structs)]
mod tests {
    use super::{Block, Volume};

    #[test]
    // TODO: Calling Core::new() multiple times seems to result in the following error message
    // followed by SIGTRAP termination:
    // g_log_set_writer_func() called multiple times
    #[ignore]
    fn test_new() {
        Volume::default();
    }

    #[tokio::test]
    #[ignore]
    async fn test_run() {
        // TODO: Is there a way to fully test run() in CI? Run pipewire/wireplumber in background?
        let _ = Volume::default().run().await;
    }
}
