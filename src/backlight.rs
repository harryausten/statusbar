//! Implementation details for block showing current backlight brightness

use anyhow::{anyhow, Context, Error};
use async_trait::async_trait;

use super::Block;

/// Backlight block private data
#[derive(Default)]
pub struct Backlight;

#[async_trait]
impl Block for Backlight {
    /// Check the current backlight brightness of the display
    ///
    /// # Errors
    ///
    /// Fails if unable to find a backlight device, or unable to read or parse the `brightness` or
    /// `max_brightness` attributes of the detected device.
    async fn run(&mut self) -> Result<String, Error> {
        let mut dir = tokio::fs::read_dir("/sys/class/backlight")
            .await
            .context("read_dir sysfs backlight")?;
        let entry = dir
            .next_entry()
            .await
            .context("Failed to parse backlight sysfs dir")?
            .ok_or_else(|| anyhow!("No backlight devices"))?;
        let mut path = entry.path();
        path.push("brightness");
        let brightness: usize = tokio::fs::read_to_string(path)
            .await
            .context("read brightness prop")?
            .trim_end()
            .parse()
            .context("parse brightness prop")?;
        path = entry.path();
        path.push("max_brightness");
        let max: usize = tokio::fs::read_to_string(path)
            .await
            .context("read max prop")?
            .trim_end()
            .parse()
            .context("parse max prop")?;
        Ok(format!("🌞 {}%", brightness * 100 / max))
    }
}

#[cfg(test)]
mod tests {
    use super::{Backlight, Block};

    #[test]
    const fn test_new() {
        let _ = Backlight;
    }

    #[tokio::test]
    async fn test_run() {
        // TODO: Is there a way to fully test run() in CI? Emulated udev devices?
        let _ = Backlight.run().await;
    }
}
