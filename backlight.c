// SPDX-License-Identifier: MIT
/*
 * Backlight brightness percentage block
 *
 * Copyright (c) 2020 Harry Austen
 */

#include <errno.h>
#include <libudev.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "block.h"

static struct udev *udev;
static struct udev_enumerate *enumerate;

int backlight_setup(void)
{
	int ret;

	udev = udev_new();
	if (!udev)
		return -errno;

	enumerate = udev_enumerate_new(udev);
	if (!enumerate)
		return -errno;

	ret = udev_enumerate_add_match_subsystem(enumerate, "backlight");
	if (ret)
		return ret;

	ret = udev_enumerate_scan_devices(enumerate);
	return ret;
}

int backlight_read(uint8_t *percentage)
{
	int ret = -ENODEV;
	struct udev_list_entry *devices;
	struct udev_list_entry *entry;

	devices = udev_enumerate_get_list_entry(enumerate);

	udev_list_entry_foreach(entry, devices) {
		const char *path = udev_list_entry_get_name(entry);
		struct udev_device *dev =
			udev_device_new_from_syspath(udev, path);
		size_t max = strtoul(
			udev_device_get_sysattr_value(dev, "max_brightness"),
			NULL, 10);
		if (max == ULONG_MAX) {
			ret = -errno;
			goto unref_device;
		}
		size_t cur = strtoul(
			udev_device_get_sysattr_value(dev, "brightness"),
			NULL, 10);
		if (max == ULONG_MAX) {
			ret = -errno;
			goto unref_device;
		}

		ret = 0;
		*percentage = (uint8_t)round((cur * 100.0) / max);
unref_device:
		udev_device_unref(dev);
		if (!ret)
			break;
	}

	return ret;
}

int backlight_exec(char *buf)
{
	int ret;
	uint8_t percentage;

	ret = backlight_setup();
	if (ret)
		goto backlight_error;

	ret = backlight_read(&percentage);
	if (ret)
		goto backlight_error;

	ret = snprintf(buf, MAX_BLOCK_SIZE, "%c%c%c%c %u%%", 0xf0,
			0x9f, 0x8c, 0x9e, percentage);
	if (ret > 0)
		ret = 0;

backlight_error:
	if (enumerate)
		udev_enumerate_unref(enumerate);
	if (udev)
		udev_unref(udev);

	return ret;
}
