// SPDX-License-Identifier: MIT
#include <errno.h>
#include <time.h>

#include "utils.h"

int timespec_sleep(struct timespec time)
{
	while (nanosleep(&time, &time) == -1)
		if (errno != EINTR)
			return -errno;
	return 0;
}

int timespec_sleep_until(const struct timespec until)
{
	struct timespec now;
	int ret = 0;

	ret = clock_gettime(CLOCK_REALTIME, &now);
	if (ret)
		return -errno;

	if (until.tv_sec == now.tv_sec) {
		if (until.tv_nsec > now.tv_nsec) {
			now.tv_sec = 0;
			now.tv_nsec = until.tv_nsec - now.tv_nsec;
			ret = timespec_sleep(now);
		}
	} else if (until.tv_sec > now.tv_sec) {
		if (until.tv_nsec >= now.tv_nsec) {
			now.tv_sec = until.tv_sec - now.tv_sec;
			now.tv_nsec = until.tv_nsec - now.tv_nsec;
		} else {
			now.tv_sec = until.tv_sec - now.tv_sec - 1;
			now.tv_nsec = 1000000000 - (now.tv_nsec - until.tv_nsec);
		}
		ret = timespec_sleep(now);
	}

	return ret;
}
