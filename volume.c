// SPDX-License-Identifier: MIT
#include <errno.h>
#include <pulse/pulseaudio.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "block.h"

struct volume_block_data {
	char *buf;
	bool finished;
};

static void sink_input_cb(__attribute__((unused)) struct pa_context *c,
			  const struct pa_sink_info *i, int is_end,
			  void *userdata)
{
	struct volume_block_data *vbd = userdata;

	if (!is_end) {
		pa_volume_t vol = pa_cvolume_avg(&i->volume);

		if (!PA_VOLUME_IS_VALID(vol))
			return;

		if (snprintf(vbd->buf, MAX_BLOCK_SIZE, "%c%c%c%c %u%%",
			     0xf0, 0x9f, 0x8e, 0xa7,
			     (vol * 100 + PA_VOLUME_NORM / 2) / PA_VOLUME_NORM
			    ) < 0)
			*vbd->buf = '\0';
	}

	vbd->finished = true;
}

static void server_info_cb(struct pa_context *c, const struct pa_server_info *i,
				void *userdata)
{
	pa_operation_unref(pa_context_get_sink_info_by_name(c,
			i->default_sink_name, sink_input_cb, userdata));
}

static void cb(struct pa_context *c, void *userdata)
{
	if (pa_context_get_state(c) != PA_CONTEXT_READY)
		return;

	pa_operation_unref(pa_context_get_server_info(
				c, server_info_cb, userdata));
}

int volume_exec(char *buf)
{
	struct pa_threaded_mainloop *ml;
	struct pa_mainloop_api *api;
	struct pa_context *context;
	int ret;
	struct volume_block_data vbd = {
		.buf = buf,
		.finished = false,
	};

	ml = pa_threaded_mainloop_new();
	if (!ml)
		puts("mainloop init returned NULL");

	api = pa_threaded_mainloop_get_api(ml);
	if (!api)
		puts("api init returned NULL");

	context = pa_context_new(api, "statusbar");
	if (!context)
		puts("context init returned NULL");

	ret = pa_context_connect(context, NULL, PA_CONTEXT_NOFLAGS, NULL);
	if (ret)
		goto error;

	pa_context_set_state_callback(context, cb, &vbd);
	ret = pa_threaded_mainloop_start(ml);
	if (ret)
		goto error;

	while (!vbd.finished)
		usleep(10);

	if (!buf[0])
		ret = -ENODEV;

error:
	pa_threaded_mainloop_lock(ml);
	pa_context_disconnect(context);
	pa_threaded_mainloop_unlock(ml);
	pa_threaded_mainloop_free(ml);
	return ret;
}
